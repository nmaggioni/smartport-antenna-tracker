#include <Arduino.h>
#include "settings.h"
#include <SPI.h>
#include <SoftwareSerial.h>
#include <NRFLite.h>
#include <data_structs.h>
#include <FrSkySportSensor.h>
#include <FrSkySportSingleWireSerial.h>
#include <_FrSkySportSensor_Rssi.h>
#include <_FrSkySportSensor_iNav.h>
#include <FrSkySportDecoder.h>

FrSkySportSensor_iNav iNav;
FrSkySportSensor_Rssi rssi;

FrSkySportDecoder decoder;
uint32_t currentTime, debugTime = 0, transmitTime = 0;

DecodedData decodedData;

const static uint8_t RADIO_ID = 1;
const static uint8_t DESTINATION_RADIO_ID = 0;
const static uint8_t PIN_RADIO_CE = 9;
const static uint8_t PIN_RADIO_CSN = 10;

NRFLite _radio;
RadioPacket _radioData;
bool _radioSuccess;

void zeroDecodedData();

void updateDecodedData(uint8_t rssi, uint16_t gpsFix, float gpsLat, float gpsLon, float baroAlt);

void printDecodedData();

void checkPacketSize();

void transmitDecodedData();

#pragma clang diagnostic push
#pragma ide diagnostic ignored "OCUnusedGlobalDeclarationInspection"
void setup() {
#ifdef SERIAL_DEBUG
    // Init serial debugging
    Serial.begin(115200);
#endif

    // Setup NRF24L01 radio
    if (!_radio.init(RADIO_ID, PIN_RADIO_CE, PIN_RADIO_CSN, NRFLite::BITRATE250KBPS,
                     RADIO_CHANNEL)) {  // 250 KBps bitrate, [2400 +] RADIO_CHANNEL Hz frequency
#ifdef SERIAL_DEBUG
        Serial.println("Cannot communicate with radio");
#endif
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wmissing-noreturn"
        while (true); // Wait here forever.
#pragma clang diagnostic pop
    }
    //_radioData.FromRadioId = RADIO_ID;

    // Setup FrSky S.Port decoder
    decoder.begin(FrSkySportSingleWireSerial::SOFT_SERIAL_PIN_2, &iNav, &rssi);
    zeroDecodedData();
}

void loop() {
    currentTime = millis();

#ifdef SERIAL_DEBUG
    // Print data every DEBUG_FREQUENCY_MS to avoid interfering with data decoding
    if (currentTime > debugTime) {
      decoder.decode();  // Pull data from bus and decode it
      debugTime = currentTime + DEBUG_FREQUENCY_MS;
      printDecodedData();
    }

    else
#endif

    // Transmit data every TRANSMIT_FREQUENCY_MS to avoid interfering with data decoding
    if (currentTime > transmitTime) {
        transmitTime = currentTime + TRANSMIT_FREQUENCY_MS;
        transmitDecodedData();
    }

        // If nothing to do in this cycle, keep updating data
    else {
        decoder.decode();  // Pull data from bus and decode it
        updateDecodedData(
                rssi.getRssi(),
                iNav.getGPSFix(),
                iNav.getGPSLat(),
                iNav.getGPSLon(),
                iNav.getBaroAlt()
        );
    }
}
#pragma clang diagnostic pop

void zeroDecodedData() {
    decodedData.rssi = 0;
    decodedData.gpsFix = 0;
    decodedData.gpsLat = 0;
    decodedData.gpsLon = 0;
    decodedData.baroAlt = 0;
}

void updateDecodedData(uint8_t rssi, uint16_t gpsFix, float gpsLat, float gpsLon, float baroAlt) {
    decodedData.rssi = rssi;
    decodedData.gpsFix = gpsFix;
    decodedData.gpsLat = gpsLat;
    decodedData.gpsLon = gpsLon;
    decodedData.baroAlt = baroAlt;
}

void printDecodedData() {
    Serial.println("");
    Serial.print("RSSI: rssi = ");
    Serial.println(decodedData.rssi); // RSSI
    { // GPS data
        Serial.print(
                "GPS: status = ");  // https://github.com/iNavFlight/inav/blob/master/docs/Telemetry.md#available-smartport-sport-sensors
        Serial.print(decodedData.gpsFix);
        Serial.print("; lat = ");
        Serial.print(decodedData.gpsLat);
        Serial.print("; lon = ");
        Serial.print(decodedData.gpsLon);
    }
    Serial.print("BAR: baro alt = ");
    Serial.println(decodedData.baroAlt); // Barometer altitude, relative to home point
    Serial.println("");
}

void checkPacketSize() {
#ifdef SERIAL_DEBUG
    Serial.print("Packet to transmit is ");
    Serial.print(sizeof(_radioData));
    Serial.println(" bytes");
#endif
}

void transmitDecodedData() {
    _radioData.OnTimeMillis = millis();
    _radioData.decodedData = decodedData;

#ifdef SERIAL_DEBUG
    checkPacketSize();
    Serial.print("Transmitting @ ");
    Serial.print(_radioData.OnTimeMillis);
    Serial.print(" ms");
#endif

    _radioSuccess = _radio.send(DESTINATION_RADIO_ID, &_radioData, sizeof(_radioData), NRFLite::NO_ACK);

#ifdef SERIAL_DEBUG
    Serial.println(_radioSuccess ? "...Success" : "...Failed");
#endif
}
