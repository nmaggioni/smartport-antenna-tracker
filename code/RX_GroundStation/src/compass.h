#pragma once

namespace Compass {

    void initCompass();

    float getCompassHeading();

}
