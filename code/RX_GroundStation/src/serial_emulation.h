#pragma once

#include <Arduino.h>
#include "data_structs.h"

/*
 * ^R0S0000L0.0l0.0A0000.0$
 * ^R68S3906L43.7821449l11.2579212A005950.22$  # HOME
 * ^R68S3906L43.8001388l11.2269834A105950.22$  # -51.14°, 17°
 * ^R68S3906L43.8001388l11.2269834A325950.22$  # -51.14°, 45°
 * ^R68S3906L43.8044788l11.2269834A325950.22$  # -45°, 42°
 * ^R68S3906L43.8044788l11.2269834A365950.22$  # -45°, 45°
 */

namespace SerialEmulation {

    bool handleRxEmulation(RadioPacket *radioPacket);

    void parseSerialPacket(String serialPacket, RadioPacket *radioPacket);

}
