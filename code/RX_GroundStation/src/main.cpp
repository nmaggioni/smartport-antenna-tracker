#include <Arduino.h>
#include "data_structs.h"
#include "compass.h"
#include "coord_utils.h"
#include "display.h"
#include "radio.h"
#include "serial_comms.h"
#include "serial_emulation.h"
#include "servos.h"
#include "settings.h"
#include "sys_utils.h"

/* --- Tracking --- */

float pan_bearing = 0;
float tilt_bearing = 0;

/* --- FrSky S.Port decoding --- */

GPS_DATA gps_data = {false, 0.0f, 0.0f, 0};
float baroAlt = 0;
GPS_DATA home_position = {false, 0.0f, 0.0f, 0};
unsigned long lastMillis = 0;
char gpsSatsBuf[3] = {'n', 'a', '\0'};
int gpsSatsDigits = -1;
bool lastRXValid = false;

void processReceivedData();

/* --- --- --- */

#pragma clang diagnostic push
#pragma ide diagnostic ignored "OCUnusedGlobalDeclarationInspection"

void setup() {
    // Delay for easier programming
    delay(3000);

    // Init serial debugging
    SerialComms::setSerialMode(SerialComms::BOOT);
    Serial.begin(115200);
    Serial.println("# < Programming delay exhausted >");
    Serial.println("# Serial debugging configured");
    Serial.println("# < Setup starting >");

    // Setup internal utils
    Serial.print("# Configuring SysUtils... ");
    SysUtils::init();
    Serial.println("OK");

    // Setup NRF24L01 radio
    Serial.print("# Configuring NRF24L01+... ");
    if (!Radio::radioInit()) {
        Serial.println("ERR");
        Serial.println("# < Cannot communicate with radio! Halting. >");
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wmissing-noreturn"
        while (true); // Wait here forever.
#pragma clang diagnostic pop
    }
    Serial.println("OK");

    // Setup compass
    Serial.print("# Configuring compass... ");
    Compass::initCompass();
    Serial.println("OK");

    // Init display
    Serial.print("# Configuring display... ");
    Display::setupDisplay();
    Serial.println("OK");
    Serial.print("# Showing splash screen for 3s... ");
    Display::showSplash(3000);
    Serial.println("OK");

    // Setup servos
    Serial.print("# Configuring servos... ");
    Servos::initServos();
    Serial.println("OK");
    Serial.print("# Pre-setting servos... ");
    pan_bearing = 0;
    tilt_bearing = 90;
    Servos::setPanServoBearing(pan_bearing);
    Servos::setTiltServoBearing(tilt_bearing);
    Serial.println("OK");

    // Setup EEPROM
    Serial.print("# Configuring EEPROM... ");
    Settings::initEEPROM();
    Serial.println("OK");
    Serial.print("# Loading settings from EEPROM... ");
    if (Settings::shouldLoadEEPROM()) {
        Settings::loadSettingsFromEEPROM();
        Serial.println("OK");
    } else {
        Serial.println("ABRT");
    }

    Serial.println("# < Setup done, enabling CLI >");
    SerialComms::setSerialMode(SerialComms::CLI);
}

void loop() {

    // Handle Serial data
    switch (SerialComms::getSerialMode()) {
        case SerialComms::BOOT:
            break;
        case SerialComms::CLI:
            if (SerialComms::handleCLI()) return; // Restart loop on Serial mode change
            break;
        case SerialComms::DBG:
            break;
        case SerialComms::EMU:
            if (SerialEmulation::handleRxEmulation(&Radio::_radioData)) {
                processReceivedData();
            }
            break;
    }

    // Receive data from TX
    while (Radio::radioHasData()) {
        Radio::radioReadData();

        if (SerialComms::getSerialMode() == SerialComms::DBG) {
            String msg = "RX @ ";
            msg += millis();
            msg += " ms (sent @ ";
            msg += Radio::_radioData.OnTimeMillis;
            msg += " ms)";
            Serial.println(msg);
        }

        processReceivedData();
    }

    // Auto-home from telemetry
    if (!home_position.valid && Settings::settings.useHomeFromTelemetry && lastRXValid) {
        if (gps_data.valid && Radio::getRawGPSFixMask() > 3006) {
            home_position = {
                    true,
                    gps_data.lat,
                    gps_data.lon,
                    baroAlt,
            };
            if (SerialComms::getSerialMode() == SerialComms::DBG || SerialComms::getSerialMode() == SerialComms::EMU) {
                Serial.println(
                        String("# Home position autoset from telemetry! (") + "Lat:" + home_position.lat + " Lon:" +
                        home_position.lon + " Alt:" +
                        home_position.alt + ")");
            }
            Display::homeFixedBeep();
        }
    }

    // Update hardware position
    if (home_position.valid && lastRXValid && gps_data.valid) {
        float dist_from_home = distance_approx(home_position.lat, home_position.lon, gps_data.lat, gps_data.lon);
        if (dist_from_home > Settings::settings.metersIgnoreFromHome * 1000) {
            pan_bearing = centerBearingOnCompassHeading(
                    pan_bearing_degrees(home_position.lat, home_position.lon, gps_data.lat, gps_data.lon),
                    Compass::getCompassHeading());
            tilt_bearing = tilt_bearing_degrees(home_position.lat, home_position.lon, home_position.alt, gps_data.lat,
                                                gps_data.lon, baroAlt);
            bool panInRange = Servos::setPanServoBearing(pan_bearing);
            bool tiltInRange = Servos::setTiltServoBearing(tilt_bearing);
            Display::servoOutOfRangeBeep(!panInRange || !tiltInRange);
        }
    }

    // Update human I/O
    Display::updateInputs();

    // Handle human I/O
    switch (Display::encoder_status) {
        default:
        case Display::NOOP:
            break;
        case Display::TURNED_CW:
            if (Display::currentLCDPage < Display::_LCD_DASHBOARD_PAGES_COUNT - 1) {
                Display::currentLCDPage++;
            } else {
                Display::currentLCDPage = 0;
            }
            break;
        case Display::TURNED_CCW:
            if (Display::currentLCDPage > 0) {
                Display::currentLCDPage--;
            } else {
                Display::currentLCDPage = Display::_LCD_DASHBOARD_PAGES_COUNT - 1;
            }
            break;
    }

    switch (Display::encoderBtn_status) {
        case ClickEncoder::Clicked:
            if (!home_position.valid && lastRXValid) {
                if (Settings::settings.ignoreGPSFixForHome || gps_data.valid) {
                    home_position = {
                            true,
                            gps_data.lat,
                            gps_data.lon,
                            baroAlt,
                    };
                    if (SerialComms::getSerialMode() == SerialComms::DBG ||
                        SerialComms::getSerialMode() == SerialComms::EMU) {
                        Serial.println(
                                String("# Home position set and locked! (") + "Lat:" + home_position.lat + " Lon:" +
                                home_position.lon + " Alt:" +
                                home_position.alt + ")");
                    }
                    digitalWrite(beeper_pin, HIGH);
                    delay(30);
                    digitalWrite(beeper_pin, LOW);
                } else if (SerialComms::getSerialMode() == SerialComms::DBG ||
                           SerialComms::getSerialMode() == SerialComms::EMU) {
                    Serial.println("# GPS fix is invalid, cannot set home.");
                }
            }
        case ClickEncoder::Held:
            break;
        case ClickEncoder::Open:
        default:
            break;
    }

    // Update display
    Display::u8g2.firstPage();
    do {
        switch (Display::currentLCDPage) {
            default:
            case Display::DATA_STREAM:
                Display::drawDataStream(lastMillis, lastRXValid, gps_data, baroAlt, gpsSatsDigits, gpsSatsBuf,
                                        home_position);
                break;
            case Display::HW_ANGLES:
                Display::drawHWAngles(home_position, pan_bearing, tilt_bearing);
                break;
        }
    } while (Display::u8g2.nextPage());

}

#pragma clang diagnostic pop

void processReceivedData() {
    lastMillis = millis();
    lastRXValid = Radio::radioValidateReceivedData();
    gpsSatsDigits = Radio::radioParseReceivedData(gpsSatsBuf, &gps_data, &baroAlt);
    if (SerialComms::getSerialMode() == SerialComms::DBG || SerialComms::getSerialMode() == SerialComms::EMU) {
        Radio::radioPrintReceivedData();
    }
}
