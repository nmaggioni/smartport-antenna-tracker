#include "radio.h"

#include <Arduino.h>
#include <NRFLite.h>

namespace Radio {

    NRFLite _radio;
    RadioPacket _radioData;

    bool radioInit() {
        return _radio.init(RADIO_ID, PIN_RADIO_CE, PIN_RADIO_CSN, NRFLite::BITRATE250KBPS,
                           RADIO_CHANNEL);  // 250 KBps bitrate, [2400 +] RADIO_CHANNEL Hz frequency
    }

    bool radioHasData() {
        return _radio.hasData();
    }

    void radioReadData() {
        _radio.readData(&_radioData);
    }

    bool radioValidateReceivedData() {
        return (_radioData.decodedData.rssi > 0);
    }

    int radioParseReceivedData(char *gpsSatsBuf, GPS_DATA *gps_data, float *baroAlt) {
        // https://github.com/iNavFlight/inav/blob/master/docs/Telemetry.md#available-smartport-sport-sensors
        int lastTwoGpsSatsDigits = _radioData.decodedData.gpsFix % 100;
        int gpsSatsDigits = lastTwoGpsSatsDigits >= 10 ? 2 : 1;
        itoa(lastTwoGpsSatsDigits, gpsSatsBuf, 10);

        gps_data->valid = _radioData.decodedData.gpsFix >= 3006;
        gps_data->lat = _radioData.decodedData.gpsLat;
        gps_data->lon = _radioData.decodedData.gpsLon;
        *baroAlt = _radioData.decodedData.baroAlt / 100.0f;

        return gpsSatsDigits;
    }

    void radioPrintReceivedData() {
        String msg = "\n--------------------------------------------------";
        msg += "\nRSSI: rssi = ";
        msg += _radioData.decodedData.rssi;
        msg += "\nGPS: status = ";
        msg += _radioData.decodedData.gpsFix;
        msg += "; lat = ";
        msg += String(_radioData.decodedData.gpsLat, 9);
        msg += "; lon = ";
        msg += String(_radioData.decodedData.gpsLon, 9);
        msg += "\nBAR: baro alt = ";
        msg += _radioData.decodedData.baroAlt;
        msg += "\n--------------------------------------------------\n";

        Serial.println(msg);
    }

    uint16_t getRawGPSFixMask() {
        return _radioData.decodedData.gpsFix;
    }

}
