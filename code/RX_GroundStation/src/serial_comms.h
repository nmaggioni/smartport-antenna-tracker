#pragma once

#include <Arduino.h>

namespace SerialComms {

    enum serial_mode {
        BOOT,
        CLI,
        EMU,
        DBG
    };

    serial_mode getSerialMode();

    void setSerialMode(serial_mode s);

    String readSerialString();

    bool handleCLI();

    void handleReset(const String &serialPacket);

}
