#pragma once

double deg2rad(double deg);
double rad2deg(double rad);
float distance_approx(double lat1, double lon1, double lat2, double lon2);
float pan_bearing_degrees(double lat1, double lon1, double lat2, double lon2);
float tilt_bearing_degrees(double lat1, double lon1, float alt1, double lat2, double lon2, float alt2);
float centerBearingOnCompassHeading(float bearing, float heading);
