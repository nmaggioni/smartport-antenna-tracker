#include "settings.h"

#include <EEPROM.h>

namespace Settings {

    bool _shouldLoadEEPROM;

    size_t configStartAddress;

    settings_s settings = {
            6,
            5,
            25,
            160,
            25,
            160,
            90,
            25,
            false,
            false,
            15,
            10,
            false,
            2,
            59,
            'E',
            false,
            true,
            5
    };

    void clearEEPROM() {
        for (size_t idx = EEPROM_START_ADDRESS; idx < sizeof(settings); idx++) {
            EEPROM.write((int) idx, 0);
        }
        EEPROM.put(EEPROM_START_ADDRESS, false);
    }

    void initEEPROM() {
        configStartAddress = sizeof(_shouldLoadEEPROM) + sizeof(settings);
    }

    bool shouldLoadEEPROM() {
        EEPROM.get(EEPROM_START_ADDRESS, _shouldLoadEEPROM);
        return _shouldLoadEEPROM;
    }

    void writeSettingsToEEPROM() {
        EEPROM.put(configStartAddress, settings);
        EEPROM.put(EEPROM_START_ADDRESS, true);
    }

    void loadSettingsFromEEPROM() {
        EEPROM.get(configStartAddress, settings);
    }

}
