#pragma once

#include <ClickEncoder.h>

/* Version shown on splash screen */

static const char VERSION[] = "1.3.0";

/* Configuration storage */

#define EEPROM_START_ADDRESS 0

/* Main screen logo */

//#define APERTURE_LOGO
//#define RADIO_LOGO
//#define THUNDER_LOGO
#define HEARTBEAT_LOGO
//#define RADIO_WIDE_LOGO

namespace Settings {

    typedef struct settings_s {
        uint8_t panServoPin;
        uint8_t tiltServoPin;
        uint8_t panServoMinDegrees;
        uint8_t panServoMaxDegrees;
        uint8_t tiltServoMinDegrees;
        uint8_t tiltServoMaxDegrees;
        int8_t panServoInitPos;
        int8_t tiltServoInitPos;
        bool panServoInvert;
        bool tiltServoInvert;
        int8_t panServoTrim;
        int8_t tiltServoTrim;
        bool declinationEnable;
        int8_t declinationMin;
        int8_t declinationSec;
        char declinationDir;
        bool ignoreGPSFixForHome;
        bool useHomeFromTelemetry;
        uint8_t metersIgnoreFromHome;
    } settings_s;
    extern settings_s settings;

    const String settings_names[] = {
            "pan_servo_pin",
            "tilt_servo_pin",
            "pan_servo_min_degrees",
            "pan_servo_max_degrees",
            "tilt_servo_min_degrees",
            "tilt_servo_max_degrees",
            "pan_servo_init_pos",
            "tilt_servo_init_pos",
            "pan_servo_invert",
            "tilt_servo_invert",
            "pan_servo_trim",
            "tilt_servo_trim",
            "declination_enable",
            "declination_minutes",
            "declination_seconds",
            "declination_direction",
            "ignore_gps_fix_for_home",
            "use_home_from_telemetry",
            "meters_ignore_from_home"
    };
    const int settings_count = 19;

    void initEEPROM();

    bool shouldLoadEEPROM();

    void clearEEPROM();

    void writeSettingsToEEPROM();

    void loadSettingsFromEEPROM();

}
