#include "compass.h"

#include <Wire.h>
#include <HMC5883L_Simple.h>
#include "settings.h"

namespace Compass {

    HMC5883L_Simple Compass;

    void initCompass() {
        Wire.begin();

        Compass.SetSamplingMode(COMPASS_CONTINUOUS);
        Compass.SetScale(COMPASS_SCALE_130);
        Compass.SetOrientation(COMPASS_VERTICAL_X_EAST);

        if (Settings::settings.declinationEnable) {
            Compass.SetDeclination(
                    Settings::settings.declinationMin,
                    Settings::settings.declinationSec,
                    Settings::settings.declinationDir
            );
        }
    }

    float getCompassHeading() {
        float heading = Compass.GetHeadingDegrees();
        if (heading > 180) {
            heading -= 360.0f;
        }
        return heading;
    }

}
