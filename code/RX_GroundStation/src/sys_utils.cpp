#include "sys_utils.h"

#include <Arduino.h>

namespace SysUtils {

    void init() {
        digitalWrite(resetPin, HIGH);
        delay(250);
        pinMode(resetPin, OUTPUT);
    }

    void reboot() {
        digitalWrite(resetPin, LOW);
    }

}
