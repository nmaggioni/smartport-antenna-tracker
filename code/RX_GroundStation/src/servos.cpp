#include "servos.h"

#include <Arduino.h>
#include <Servo.h>
#include "settings.h"

Servo panServo;
Servo tiltServo;

int tilt_start;

namespace Servos {

    void initServos() {
        panServo.attach(Settings::settings.panServoPin);
        tiltServo.attach(Settings::settings.tiltServoPin);

        tilt_start = Settings::settings.tiltServoMinDegrees + Settings::settings.tiltServoTrim;

        panServo.write(Settings::settings.panServoInitPos + Settings::settings.panServoTrim);
        tiltServo.write(Settings::settings.tiltServoInitPos + Settings::settings.tiltServoTrim);
    }

    bool setPanServoBearing(float bearing) {
        if (bearing < -90) {
            bearing = -90;
        } else if (bearing > 90) {
            bearing = 90;
        }
        float mapped_bearing = bearing + 90 + Settings::settings.panServoTrim;

        bool isInRange = true;
        if (mapped_bearing < Settings::settings.panServoMinDegrees) {
            isInRange = false;
            mapped_bearing = Settings::settings.panServoMinDegrees;
        } else if (mapped_bearing > Settings::settings.panServoMaxDegrees) {
            isInRange = false;
            mapped_bearing = Settings::settings.panServoMaxDegrees;
        }

        if (Settings::settings.panServoInvert) {
            mapped_bearing = map((long) mapped_bearing,
                                 Settings::settings.panServoMinDegrees, Settings::settings.panServoMaxDegrees,
                                 Settings::settings.panServoMaxDegrees, Settings::settings.panServoMinDegrees
            );
        }

        panServo.write((int) mapped_bearing);
        return isInRange;
    }

    bool setTiltServoBearing(float bearing) {
        if (bearing < 0) {
            bearing = 0;
        } else if (bearing > 90) {
            bearing = 90;
        }

        long range_start = tilt_start;
        long range_end = (long) (tilt_start + 90 - Settings::settings.tiltServoTrim / 1.5);

        float mapped_bearing = map((long) bearing, 0, 90,
                                   !Settings::settings.tiltServoInvert ? range_start : range_end,
                                   !Settings::settings.tiltServoInvert ? range_end : range_start);

        bool isInRange = true;
        if (mapped_bearing < Settings::settings.tiltServoMinDegrees) {
            isInRange = false;
            mapped_bearing = Settings::settings.tiltServoMinDegrees;
        } else if (mapped_bearing > Settings::settings.tiltServoMaxDegrees) {
            isInRange = false;
            mapped_bearing = Settings::settings.tiltServoMaxDegrees;
        }

        tiltServo.write((int) mapped_bearing);
        return isInRange;
    }

}
