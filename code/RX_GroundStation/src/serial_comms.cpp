#include "serial_comms.h"

#include "settings.h"
#include "sys_utils.h"

namespace SerialComms {

    serial_mode current_serial_mode = BOOT;

    serial_mode getSerialMode() {
        return current_serial_mode;
    }

    void setSerialMode(serial_mode s) {
        current_serial_mode = s;
    }

    String readSerialString() {
        String serialBuffer;
        char serialBufferChar;

        while (true) {
            while (Serial.available() > 0) {
                serialBufferChar = (char) Serial.read();
                if (serialBufferChar == '\n') {
                    return serialBuffer;
                }
                serialBuffer += serialBufferChar;
            }
        }
    }

    void parseSetCommand(const String &serialPacket, String (&params)[2]) {
        String paramName = "";
        String paramValue = "";
        bool separatorFound = false;

        for (unsigned int i = 0; i < serialPacket.length(); ++i) {
            switch (serialPacket.charAt(i)) {
                case ' ':
                case '=':
                    separatorFound = true;
                    continue;
                default:
                    if (!separatorFound) {
                        paramName += String(serialPacket.charAt(i));
                    } else {
                        paramValue += String(serialPacket.charAt(i));
                    }
            }
        }
        params[0] = paramName;
        params[1] = paramValue;
    }

    bool strToBool(const String &s) {
        return (s.length() >= 2 && s.length() <= 3 && s == "ON");
    }

    String boolToStr(const bool b) {
        return b ? String("ON") : String("OFF");
    }

    void handleReset(const String &serialPacket) {
        if (serialPacket == "!RST") {
            Serial.println("# Rebooting board...");
            Serial.flush();
            delay(250);
            SysUtils::reboot();
        }
    }

    // Returns true if current loop must be restarted
    bool handleCLI(const String &serialPacket) {
        // Handle mode changes
        if (serialPacket.startsWith("!")) {
            if (serialPacket == "!DBG") {
                Serial.println("# Entering DEBUG mode");
                setSerialMode(DBG);
                return true;
            } else if (serialPacket == "!EMU") {
                Serial.println("# Entering EMULATOR mode");
                setSerialMode(EMU);
                return true;
            } else {
                handleReset(serialPacket);
            }
        }

        if (serialPacket.startsWith("get ") || serialPacket.startsWith("set ")) {
            String params[2];
            parseSetCommand(serialPacket.substring(4), params);

            // Handle get commands
            if (serialPacket.startsWith("get ")) {
                Serial.print(String("# ") + params[0] + String(" = "));

                if (params[0] == "pan_servo_pin") {
                    Serial.println(String(Settings::settings.panServoPin));
                    return false;
                } else if (params[0] == "tilt_servo_pin") {
                    Serial.println(String(Settings::settings.tiltServoPin));
                    return false;
                } else if (params[0] == "pan_servo_min_degrees") {
                    Serial.println(String(Settings::settings.panServoMinDegrees));
                    return false;
                } else if (params[0] == "pan_servo_max_degrees") {
                    Serial.println(String(Settings::settings.panServoMaxDegrees));
                    return false;
                } else if (params[0] == "tilt_servo_min_degrees") {
                    Serial.println(String(Settings::settings.tiltServoMinDegrees));
                    return false;
                } else if (params[0] == "tilt_servo_max_degrees") {
                    Serial.println(String(Settings::settings.tiltServoMaxDegrees));
                    return false;
                } else if (params[0] == "pan_servo_init_pos") {
                    Serial.println(String(Settings::settings.panServoInitPos));
                    return false;
                } else if (params[0] == "tilt_servo_init_pos") {
                    Serial.println(String(Settings::settings.tiltServoInitPos));
                    return false;
                } else if (params[0] == "pan_servo_invert") {
                    Serial.println(boolToStr(Settings::settings.panServoInvert));
                    return false;
                } else if (params[0] == "tilt_servo_invert") {
                    Serial.println(boolToStr(Settings::settings.tiltServoInvert));
                    return false;
                } else if (params[0] == "pan_servo_trim") {
                    Serial.println(String(Settings::settings.panServoTrim));
                    return false;
                } else if (params[0] == "tilt_servo_trim") {
                    Serial.println(String(Settings::settings.tiltServoTrim));
                    return false;
                } else if (params[0] == "declination_enable") {
                    Serial.println(boolToStr(Settings::settings.declinationEnable));
                    return false;
                } else if (params[0] == "declination_minutes") {
                    Serial.println(String(Settings::settings.declinationMin));
                    return false;
                } else if (params[0] == "declination_seconds") {
                    Serial.println(String(Settings::settings.declinationSec));
                    return false;
                } else if (params[0] == "declination_direction") {
                    Serial.println(String(Settings::settings.declinationDir));
                    return false;
                } else if (params[0] == "ignore_gps_fix_for_home") {
                    Serial.println(boolToStr(Settings::settings.ignoreGPSFixForHome));
                    return false;
                } else if (params[0] == "use_home_from_telemetry") {
                    Serial.println(boolToStr(Settings::settings.useHomeFromTelemetry));
                    return false;
                } else if (params[0] == "meters_ignore_from_home") {
                    Serial.println(String(Settings::settings.metersIgnoreFromHome));
                    return false;
                }
            }

            // Handle set commands
            if (serialPacket.startsWith("set ")) {

                if (params[0] == "pan_servo_pin") {
                    Settings::settings.panServoPin = (uint8_t) atoi(params[1].c_str()); // NOLINT(cert-err34-c)
                    return handleCLI(String("get ") + params[0]);
                } else if (params[0] == "tilt_servo_pin") {
                    Settings::settings.tiltServoPin = (uint8_t) atoi(params[1].c_str()); // NOLINT(cert-err34-c)
                    return handleCLI(String("get ") + params[0]);
                } else if (params[0] == "pan_servo_min_degrees") {
                    Settings::settings.panServoMinDegrees = (uint8_t) atoi(params[1].c_str()); // NOLINT(cert-err34-c)
                    return handleCLI(String("get ") + params[0]);
                } else if (params[0] == "pan_servo_max_degrees") {
                    Settings::settings.panServoMaxDegrees = (uint8_t) atoi(params[1].c_str()); // NOLINT(cert-err34-c)
                    return handleCLI(String("get ") + params[0]);
                } else if (params[0] == "tilt_servo_min_degrees") {
                    Settings::settings.tiltServoMinDegrees = (uint8_t) atoi(params[1].c_str()); // NOLINT(cert-err34-c)
                    return handleCLI(String("get ") + params[0]);;
                } else if (params[0] == "tilt_servo_max_degrees") {
                    Settings::settings.tiltServoMaxDegrees = (uint8_t) atoi(params[1].c_str()); // NOLINT(cert-err34-c)
                    return handleCLI(String("get ") + params[0]);
                } else if (params[0] == "pan_servo_init_pos") {
                    Settings::settings.panServoInitPos = (int8_t) atoi(params[1].c_str()); // NOLINT(cert-err34-c)
                    return handleCLI(String("get ") + params[0]);
                } else if (params[0] == "tilt_servo_init_pos") {
                    Settings::settings.tiltServoInitPos = (int8_t) atoi(params[1].c_str()); // NOLINT(cert-err34-c)
                    return handleCLI(String("get ") + params[0]);
                } else if (params[0] == "pan_servo_invert") {
                    Settings::settings.panServoInvert = strToBool(params[1]);
                    return handleCLI(String("get ") + params[0]);
                } else if (params[0] == "tilt_servo_invert") {
                    Settings::settings.tiltServoInvert = strToBool(params[1]);
                    return handleCLI(String("get ") + params[0]);
                } else if (params[0] == "pan_servo_trim") {
                    Settings::settings.panServoTrim = (int8_t) atoi(params[1].c_str()); // NOLINT(cert-err34-c)
                    return handleCLI(String("get ") + params[0]);
                } else if (params[0] == "tilt_servo_trim") {
                    Settings::settings.tiltServoTrim = (int8_t) atoi(params[1].c_str()); // NOLINT(cert-err34-c)
                    return handleCLI(String("get ") + params[0]);
                } else if (params[0] == "declination_enable") {
                    Settings::settings.declinationEnable = strToBool(params[1]);
                    return handleCLI(String("get ") + params[0]);
                } else if (params[0] == "declination_minutes") {
                    Settings::settings.declinationMin = (int8_t) atoi(params[1].c_str()); // NOLINT(cert-err34-c)
                    return handleCLI(String("get ") + params[0]);
                } else if (params[0] == "declination_seconds") {
                    Settings::settings.declinationSec = (int8_t) atoi(params[1].c_str()); // NOLINT(cert-err34-c)
                    return handleCLI(String("get ") + params[0]);
                } else if (params[0] == "declination_direction") {
                    Settings::settings.declinationDir = params[1].charAt(0); // NOLINT(cert-err34-c)
                    return handleCLI(String("get ") + params[0]);
                } else if (params[0] == "ignore_gps_fix_for_home") {
                    Settings::settings.ignoreGPSFixForHome = strToBool(params[1]);
                    return handleCLI(String("get ") + params[0]);
                } else if (params[0] == "use_home_from_telemetry") {
                    Settings::settings.useHomeFromTelemetry = strToBool(params[1]);
                    return handleCLI(String("get ") + params[0]);
                } else if (params[0] == "meters_ignore_from_home") {
                    Settings::settings.metersIgnoreFromHome = (uint8_t) atoi(params[1].c_str()); // NOLINT(cert-err34-c)
                    return handleCLI(String("get ") + params[0]);
                }
            }
        }

            // Handle global commands
        else if (serialPacket == "dump") {
            Serial.println("/* --- --- --- --- --- --- */");
            for (const String &settings_name : Settings::settings_names) {
                handleCLI(String("get ") + settings_name);
            }
            Serial.println("/* --- --- --- --- --- --- */");
            return false;
        } else if (serialPacket == "save") {
            Settings::writeSettingsToEEPROM();
            Serial.println("# Settings saved to EEPROM");
            return false;
        } else if (serialPacket == "clear") {
            Settings::clearEEPROM();
            Serial.println("# EEPROM cleared, next boot will load defaults");
            return false;
        }

        // Warn if command didn't match anything
        Serial.println("# Unknown command");
        return false;
    }

    bool handleCLI() {
        if (Serial.available() > 0) {
            return handleCLI(readSerialString());
        }
        return false;
    }

}
