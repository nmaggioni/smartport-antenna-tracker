#include "coord_utils.h"

#include <math.h>

const double pi = 3.14159;
const double R = 6378.388;

double deg2rad(double deg) {
    return deg * pi / 180;
}

double rad2deg(double rad) {
    return rad * 180 / pi;
}

float distance_approx(double lat1, double lon1, double lat2, double lon2) {
    if (lat1 == lat2 && lon1 == lon2) return 0.0f;
    double _lat1 = deg2rad(lat1);
    double _lon1 = deg2rad(lon1);
    double _lat2 = deg2rad(lat2);
    double _lon2 = deg2rad(lon2);

    double x = (_lon2 - _lon1) * cos((_lat1 + _lat2) / 2);
    double y = (_lat1 > _lat2 ? _lat1 - _lat2 : _lat2 - _lat1);
    return sqrt(x * x + y * y) * R;
}

float pan_bearing_degrees(double lat1, double lon1, double lat2, double lon2) {
    double _lat1 = deg2rad(lat1);
    double _lon1 = deg2rad(lon1);
    double _lat2 = deg2rad(lat2);
    double _lon2 = deg2rad(lon2);

    double y = sin(_lon2 - _lon1) * cos(_lat2);
    double x = cos(_lat1) * sin(_lat2) - sin(_lat1) * cos(_lat2) * cos(_lon2 - _lon1);
    double bearing = atan2(y, x);

    return rad2deg(bearing);
}

float tilt_bearing_degrees(double lat1, double lon1, float alt1, double lat2, double lon2, float alt2) {
    if (alt1 == alt2) return 0.0f;
    double alt = (alt1 > alt2 ? alt1 - alt2 : alt2 - alt1);
    double hyp = sqrt(pow(distance_approx(lat1, lon1, lat2, lon2) * 1000, 2) + pow(alt, 2));
    long angle = (long) rad2deg(asin(alt / hyp));
    return (alt2 > alt1) ? angle : -angle;
}

float centerBearingOnCompassHeading(float bearing, float heading) {
    if ((heading > 0 && bearing < heading) || (heading < 0 && bearing > heading)) {
        return -(heading - bearing);
    }
    return bearing - heading;
}
