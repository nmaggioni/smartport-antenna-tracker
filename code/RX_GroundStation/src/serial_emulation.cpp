#include "serial_emulation.h"

#include "serial_comms.h"

namespace SerialEmulation {

    bool handleRxEmulation(RadioPacket *radioPacket) {
        if (Serial.available() > 0) {
            String serialPacket = SerialComms::readSerialString();
            SerialComms::handleReset(serialPacket);
            Serial.println("SERIAL_EMULATION: Packet received");
            parseSerialPacket(serialPacket, radioPacket);
            return true;
        }
        return false;
    }

    void parseSerialPacket(String serialPacket, RadioPacket *radioPacket) {
        int iRssi = serialPacket.indexOf('R');
        int iGpsFix = serialPacket.indexOf('S');
        int iLatitude = serialPacket.indexOf('L');
        int iLongitude = serialPacket.indexOf('l');
        int iAltitude = serialPacket.indexOf('A');
        int iEnd = serialPacket.indexOf('$');
        if (iRssi == -1 || iGpsFix == -1 || iLatitude == -1 || iLongitude == -1 || iAltitude == -1) {
            Serial.println("SERIAL_EMULATION: Discarding bogus packet, missing delimiters");
            return;
        }
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wsign-conversion"
        String rssi = serialPacket.substring(iRssi + 1, iGpsFix);
        String gpsfix = serialPacket.substring(iGpsFix + 1, iLatitude);
        String latitude = serialPacket.substring(iLatitude + 1, iLongitude);
        String longitude = serialPacket.substring(iLongitude + 1, iAltitude);
        String altitude = serialPacket.substring(iAltitude + 1, iEnd);
#pragma clang diagnostic pop
        if (rssi.length() == 0 || gpsfix.length() == 0 || latitude.length() == 0 ||
            longitude.length() == 0 || altitude.length() == 0) {
            Serial.println("SERIAL_EMULATION: Discarding bogus packet, emtpy fields");
            return;
        }

        radioPacket->OnTimeMillis = 0;
        radioPacket->decodedData.rssi = (uint8_t) atoi(rssi.c_str());
        radioPacket->decodedData.gpsFix = (uint16_t) atoi(gpsfix.c_str());
        radioPacket->decodedData.gpsLat = atof(latitude.c_str());
        radioPacket->decodedData.gpsLon = atof(longitude.c_str());
        radioPacket->decodedData.baroAlt = (float) atof(altitude.c_str());
    }

}
