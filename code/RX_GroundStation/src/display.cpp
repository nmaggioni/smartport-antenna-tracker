#include "display.h"

#include <TimerOne.h>

#include "coord_utils.h"
#include "settings.h"

namespace Display {

    U8G2_ST7920_128X64_2_SW_SPI u8g2(U8G2_R0, 23, 17, 16);
    char lastMillisBuf[10];
    ClickEncoder *encoder;
    int16_t encoderValue = 0;
    int16_t encoderLastValue = 0;
    uint32_t encoderLastMillisChanged = 0;
    bool stopBtn_status = false;
    ClickEncoder::Button_e encoderBtn_status = ClickEncoder::Open;
    encoderStatus_t encoder_status = NOOP;
    int currentLCDPage = DATA_STREAM;
    long servoOutOfRangeBeepStartMillis = 0;
    long servoOutOfRangeBeepLastMillis = 0;
    bool servoOutOfRangeIsBeeping = false;

    void timerIsr() {
        encoder->service();
    }

    void setupDisplay() {
        pinMode(beeper_pin, OUTPUT);
        pinMode(stopBtn_pin, INPUT_PULLUP);
        encoder = new ClickEncoder(encoder1_pin, encoder2_pin, encoderBtn_pin, 1);
        encoder->setAccelerationEnabled(false);
        Timer1.initialize(1000);
        Timer1.attachInterrupt(timerIsr);

        u8g2.begin();

        stopBtn_status = false;
    }

    void showSplash(unsigned int duration) {
        u8g2.firstPage();
        do {
            u8g2.drawFrame(0, 0, 128, 64);
            u8g2.drawFrame(1, 1, 126, 62);
            u8g2.drawFrame(2, 2, 124, 60);
            u8g2.setFont(u8g2_font_timR08_tf);
            drawCenteredStr(15, (char *) "SMARTPORT");
            drawCenteredStr(30, (char *) "ANTENNA  TRACKER");
            u8g2.setFont(u8g2_font_timR10_tf);
            drawCenteredStr(55, (char *) (String("v") + String(VERSION)).c_str());
        } while (u8g2.nextPage());
        delay(duration);
        u8g2.clearDisplay();
    }

    void drawCenteredStr(int row, char *string) {
        u8g2.drawStr((u8g2_uint_t) ((128 / 2) - u8g2.getStrWidth(string) / 2), (u8g2_uint_t) row, string);
    }

    void drawLogo() {
#ifdef APERTURE_LOGO
        u8g2.setFont(u8g2_font_open_iconic_app_4x_t);
        u8g2.drawGlyph(91, 59, 0x40);
#elif defined RADIO_LOGO
        u8g2.setFont(u8g2_font_open_iconic_embedded_4x_t);
        u8g2.drawGlyph(93, 59, 0x50);
#elif defined THUNDER_LOGO
        u8g2.setFont(u8g2_font_open_iconic_embedded_4x_t);
        u8g2.drawGlyph(99, 60, 0x43);
#elif defined HEARTBEAT_LOGO
        u8g2.setFont(u8g2_font_open_iconic_embedded_4x_t);
        u8g2.drawGlyph(91, 63, 0x46);
#elif defined RADIO_WIDE_LOGO
        u8g2.setFont(u8g2_font_open_iconic_mime_4x_t);
        u8g2.drawGlyph(91, 65, 0x40);
#endif
    }

    void drawDataStream(long lastMillis, bool lastRXValid, GPS_DATA gps_data, float baroAlt, int gpsSatsDigits,
                        char *gpsSatsBuf, GPS_DATA home_position) {
        u8g2.setFont(u8g2_font_5x7_tf);

        u8g2.drawStr(2, 8, "MS Since Last RX:");
        ltoa(millis() - lastMillis, lastMillisBuf, 10);
        u8g2.drawStr(90, 8, lastMillisBuf);

        u8g2.drawStr(2, 17, "RX Valid:");
        u8g2.setFont(u8g2_font_open_iconic_check_1x_t);
        u8g2.drawGlyph(51, 18, lastRXValid ? 0x40 : 0x44);
        u8g2.setFont(u8g2_font_5x7_tf);

        u8g2.drawStr(2, 26, "GPS Sats:");
        u8g2.setFont(u8g2_font_open_iconic_all_1x_t);
        u8g2.drawGlyph(51, 27, gps_data.valid ? 0x73 : 0x11B);
        u8g2.setFont(u8g2_font_5x7_tf);
        u8g2.drawStr(63, 26, "(");
        u8g2.drawStr((u8g2_uint_t) (gpsSatsDigits == -1 ? 68 : 67), 26, gpsSatsBuf);
        int gpsSatsDigitsOffset = 0;
        switch (gpsSatsDigits) {
            case -1:
                gpsSatsDigitsOffset = 6;
                break;
            case 1:
                gpsSatsDigitsOffset = 0;
                break;
            case 2:
                gpsSatsDigitsOffset = 5;
                break;
            default:
                break;
        }
        u8g2.drawStr((u8g2_uint_t) (72 + gpsSatsDigitsOffset), 26, ")");

        u8g2.setFont(u8g2_font_open_iconic_embedded_1x_t);
        u8g2.drawGlyph(97, 22, 0x44);
        u8g2.setFont(u8g2_font_5x7_tf);
        u8g2.drawStr(105, 21, ":");
        u8g2.setFont(u8g2_font_open_iconic_check_1x_t);
        u8g2.drawGlyph(113, 22, home_position.valid ? 0x40 : 0x44);
        u8g2.setFont(u8g2_font_5x7_tf);

        u8g2.drawStr(2, 44, "LAT:");
        String lat = ((int) gps_data.lat > 0 && gps_data.lat < 10 ? String(" ") : String("")) + String(gps_data.lat, 7);
        u8g2.drawStr(25, 44, (gps_data.lat != 0.0f ? lat + "d" : String("n/a")).c_str());

        u8g2.drawStr(2, 53, "LON:");
        String lon = ((int) gps_data.lon > 0 && gps_data.lon < 10 ? String(" ") : String("")) + String(gps_data.lon, 7);
        u8g2.drawStr(25, 53, (gps_data.lon != 0.0f ? lon + "d" : String("n/a")).c_str());

        u8g2.drawStr(2, 62, "ALT:");
        u8g2.drawStr(25, 62, (String(baroAlt, 1) + "m").c_str());

        drawLogo();
    }

    void drawHWAngles(GPS_DATA home_position, float pan_bearing, float tilt_bearing) {
        u8g2.setFont(u8g2_font_5x7_tf);

        u8g2.drawStr(2, 8, "PAN ANGLE:");
        u8g2.drawStr(55, 8, (home_position.valid ? String(pan_bearing) + "d" : String("home not set")).c_str());

        u8g2.drawStr(1, 17, "TILT ANGLE:");
        u8g2.drawStr(59, 17, (home_position.valid ? String(tilt_bearing) + "d" : String("home not set")).c_str());

        u8g2.drawCircle(35, 40, 20);
        u8g2.drawDisc(35, 40, 3);
        u8g2.drawLine(35, 40,
                      (u8g2_uint_t) (36 - (cos(deg2rad(pan_bearing + 90)) * 20)),
                      (u8g2_uint_t) (41 - (sin(deg2rad(pan_bearing + 90)) * 20))
        );
        u8g2.setFontDirection(1);
        u8g2.drawStr(3, 30, "P A N");
        u8g2.setFontDirection(0);
        u8g2.setFont(u8g2_font_micro_tr);
        u8g2.drawStr(34, 27, "0");
        u8g2.drawStr(47, 43, "90");
        u8g2.drawStr(30, 59, "180");
        u8g2.drawStr(17, 43, "270");
        u8g2.setFont(u8g2_font_5x7_tf);

        u8g2.drawHLine(75, 60, 35);
        u8g2.drawVLine(110, 25, 36);
        u8g2.drawLine(109, 59,
                      (u8g2_uint_t) (110 - (cos(deg2rad(tilt_bearing)) * 35)),
                      (u8g2_uint_t) (59 - (sin(deg2rad(tilt_bearing)) * 35))
        );
        u8g2.setFontDirection(3);
        u8g2.drawStr(122, 60, "T I L T");
        u8g2.setFontDirection(0);
        u8g2.setFont(u8g2_font_micro_tr);
        u8g2.drawStr(70, 62, "0");
        u8g2.drawStr(76, 32, "45");
        u8g2.drawStr(107, 24, "90");
    }

    bool readStopBtn() {
        return !digitalRead(stopBtn_pin);
    }

    ClickEncoder::Button_e readEncoderBtn() {
        return encoder->getButton();
    }

    encoderStatus_t readEncoderValue() {
        encoderValue += encoder->getValue();
        if (encoderValue != encoderLastValue) {
            encoderStatus_t encoderDirection = encoderValue > encoderLastValue ? TURNED_CW : TURNED_CCW;
            encoderLastValue = encoderValue;
            return encoderDirection;
        }
        return NOOP;
    }

    encoderStatus_t filterEncoderReadings(encoderStatus_t status) {
        if (millis() > encoderLastMillisChanged + encoderRotationDelay) {
            encoderLastMillisChanged = millis();
            return status;
        }
        return NOOP;
    }

    void updateInputs() {
        stopBtn_status = readStopBtn();
        encoderBtn_status = readEncoderBtn();
        encoder_status = filterEncoderReadings(readEncoderValue());
        //encoder_status = readEncoderValue();
    }

    void beep(bool enable) {
        digitalWrite(beeper_pin, (uint8_t) (enable ? HIGH : LOW));
    }

    void servoOutOfRangeBeep(bool enable) {
        long currentMillis = millis();
        if (!enable && (servoOutOfRangeIsBeeping || servoOutOfRangeBeepStartMillis != 0)) {
            beep(false);
            servoOutOfRangeIsBeeping = false;
            servoOutOfRangeBeepStartMillis = 0;
            servoOutOfRangeBeepLastMillis = 0;
        }
        if (enable) {
            if (servoOutOfRangeBeepStartMillis == 0) {
                servoOutOfRangeBeepStartMillis = currentMillis;
            } else if (currentMillis > servoOutOfRangeBeepStartMillis + 15000 &&
                currentMillis > servoOutOfRangeBeepLastMillis + (servoOutOfRangeIsBeeping ? 250 : 2750)
                    ) {
                beep(!servoOutOfRangeIsBeeping);
                servoOutOfRangeIsBeeping = !servoOutOfRangeIsBeeping;
                servoOutOfRangeBeepLastMillis = currentMillis;
            }
        }
    }

    void homeFixedBeep() {
        for (int i = 0; i < 6; ++i) {
            if (i == 3) delay(60);
            beep(true);
            delay(30);
            beep(false);
            delay(30);
        }
    }

}
