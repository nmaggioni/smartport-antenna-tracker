#pragma once

namespace Servos {

    void initServos();

    bool setPanServoBearing(float bearing);

    bool setTiltServoBearing(float bearing);

}
