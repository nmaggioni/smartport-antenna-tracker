#pragma once

#include "data_structs.h"

#define RADIO_ID 0
#define PIN_RADIO_CE 53
#define PIN_RADIO_CSN 42

namespace Radio {

    extern RadioPacket _radioData;

    bool radioInit();

    bool radioHasData();

    void radioReadData();

    bool radioValidateReceivedData();

    int radioParseReceivedData(char *gpsSatsBuf, GPS_DATA *gps_data, float *baroAlt);

    void radioPrintReceivedData();

    uint16_t getRawGPSFixMask();

}
