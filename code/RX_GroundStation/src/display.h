#pragma once

#include <U8g2lib.h>
#include <ClickEncoder.h>

#include "data_structs.h"

/*
   RRGLCD library reference:
   https://github.com/olikraus/u8g2/wiki/u8g2reference

   RRGLCD schematics & pinouts:
   https://reprap.org/mediawiki/images/5/51/RRD_FULL_GRAPHIC_SMART_CONTROLER_SCHEMATIC.pdf
   https://reprap.org/mediawiki/images/7/79/LCD_connect_SCHDOC.pdf

   RAMPS schematics & pinouts:
   https://reprap.org/mediawiki/images/f/f6/RAMPS1.4schematic.png
   https://reprap.org/mediawiki/images/c/ca/Arduinomega1-4connectors.png
*/

#define beeper_pin 37
#define stopBtn_pin 41
#define encoderBtn_pin 35
#define encoder1_pin 33
#define encoder2_pin 31

#define encoderRotationDelay 150

namespace Display {

    extern U8G2_ST7920_128X64_2_SW_SPI u8g2;
    extern bool stopBtn_status;
    extern ClickEncoder::Button_e encoderBtn_status;
    typedef enum {
        NOOP,
        TURNED_CW,
        TURNED_CCW
    } encoderStatus_t;
    extern encoderStatus_t encoder_status;
    typedef enum {
        DATA_STREAM = 0,
        HW_ANGLES,
        _LCD_DASHBOARD_PAGES_COUNT,
    } lcd_dashboard_pages_t;
    extern int currentLCDPage;

    void setupDisplay();

    void showSplash(unsigned int duration);

    void drawCenteredStr(int row, char *string);

    void drawLogo();

    void drawDataStream(long lastMillis, bool lastRXValid, GPS_DATA gps_data, float baroAlt, int gpsSatsDigits,
                        char *gpsSatsBuf, GPS_DATA home_position);

    void drawHWAngles(GPS_DATA home_position, float pan_bearing, float tilt_bearing);

    void updateInputs();

    encoderStatus_t readEncoderValue();

    void beep(bool enable);

    void servoOutOfRangeBeep(bool enable);

    void homeFixedBeep();

};
