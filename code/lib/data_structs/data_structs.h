#pragma once
#include <stdint.h>

const static uint8_t RADIO_CHANNEL = 150;

struct DecodedData {
  uint8_t rssi;
  uint16_t gpsFix;
  double gpsLat;
  double gpsLon;
  float baroAlt;
};

struct RadioPacket { // Up to 32 bytes
  uint32_t OnTimeMillis;
  DecodedData decodedData;
};

struct GPS_DATA {
  bool valid;
  double lat;
  double lon;
  float alt;
};
