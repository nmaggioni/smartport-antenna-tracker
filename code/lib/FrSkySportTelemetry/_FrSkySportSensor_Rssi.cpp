#include "_FrSkySportSensor_Rssi.h" 

FrSkySportSensor_Rssi::FrSkySportSensor_Rssi(SensorId id) : FrSkySportSensor(id) { }

uint16_t FrSkySportSensor_Rssi::decodeData(uint8_t id, uint16_t appId, uint32_t data)
{
  if((sensorId == id) || (sensorId == FrSkySportSensor::ID_IGNORE))
  {
    switch(appId)
    {
      case RSSI_DATA_ID:
        rssi = (uint8_t)data;
        return appId;
    }
  }
  return SENSOR_NO_DATA_ID;
}

uint8_t FrSkySportSensor_Rssi::getRssi() { return rssi; }
