#ifndef __FRSKY_SPORT_SENSOR_RSSI_H_
#define __FRSKY_SPORT_SENSOR_RSSI_H_

#include "FrSkySportSensor.h"

#define RSSI_DEFAULT_ID ID25

#define RSSI_DATA_ID          0xF101

class FrSkySportSensor_Rssi : public FrSkySportSensor
{
  public:
    FrSkySportSensor_Rssi(SensorId id = RSSI_DEFAULT_ID);
    virtual uint16_t decodeData(uint8_t id, uint16_t appId, uint32_t data);
    uint8_t getRssi();
    
  private:
    uint8_t rssi;
    
};

#endif // __FRSKY_SPORT_SENSOR_RSSI_H_
