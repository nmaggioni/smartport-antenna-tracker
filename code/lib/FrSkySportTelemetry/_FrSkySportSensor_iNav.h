#ifndef __FRSKY_SPORT_SENSOR_INAV_H_
#define __FRSKY_SPORT_SENSOR_INAV_H_

#include "FrSkySportSensor.h"

#define INAV_DEFAULT_ID ID28

#define ALT_DATA_ID 0x0100
#define GPS_FIX_DATA_ID 0x0410
#define HOME_DIST_DATA_ID 0x0420
#define GPS_LAT_LON_DATA_ID   0x0800
#define GPS_ALT_DATA_ID       0x0820
#define HDG_DATA_ID 0x0840

class FrSkySportSensor_iNav : public FrSkySportSensor
{
  public:
    FrSkySportSensor_iNav(SensorId id = INAV_DEFAULT_ID);

    virtual uint16_t decodeData(uint8_t id, uint16_t appId, uint32_t data);
    float getBaroAlt();
    uint16_t getGPSFix();  // https://github.com/iNavFlight/inav/blob/master/docs/Telemetry.md#available-smartport-sport-sensors
    float getHomeDist();
    float getGPSLat();
    float getGPSLon();
    float getGPSAlt();
    float getHeading();

  private:
    float baroAlt;
    uint16_t gpsFix;
    float homeDist;
    float gpsLat;
    float gpsLon;
    float gpsAlt;
    float heading;
};

#endif // __FRSKY_SPORT_SENSOR_INAV_H_
