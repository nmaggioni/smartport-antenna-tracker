#include "_FrSkySportSensor_iNav.h"

FrSkySportSensor_iNav::FrSkySportSensor_iNav(SensorId id) : FrSkySportSensor(id) { }

uint16_t FrSkySportSensor_iNav::decodeData(uint8_t id, uint16_t appId, uint32_t data) {
  if((sensorId == id) || (sensorId == FrSkySportSensor::ID_IGNORE))
  {
    switch(appId)
    {
	  case ALT_DATA_ID:
        baroAlt = data;
        return appId;
      case GPS_FIX_DATA_ID:
		gpsFix = data;
		return appId;
      case HOME_DIST_DATA_ID:
        homeDist = data;
        return appId;
      case GPS_LAT_LON_DATA_ID:
        {
          float latLonData = (data & 0x3FFFFFFF) / 10000.0 / 60.0;
          if((data & 0x40000000) > 0) latLonData = -latLonData;                 // is negative?
          if((data & 0x80000000) == 0) gpsLat = latLonData; else gpsLon = latLonData; // is latitude?
        }
        return appId;
      case GPS_ALT_DATA_ID:
        gpsAlt = ((int32_t)data) / 100.0;
        return appId;
      case HDG_DATA_ID:
        heading = data / 100.0;
        return appId;
    }
  }
  return SENSOR_NO_DATA_ID;
}

float FrSkySportSensor_iNav::getBaroAlt() { return baroAlt; }
uint16_t FrSkySportSensor_iNav::getGPSFix() { return gpsFix; }
float FrSkySportSensor_iNav::getHomeDist() { return homeDist; }
float FrSkySportSensor_iNav::getGPSLat() { return gpsLat; }
float FrSkySportSensor_iNav::getGPSLon() { return gpsLon; }
float FrSkySportSensor_iNav::getGPSAlt() { return gpsAlt; }
float FrSkySportSensor_iNav::getHeading() { return heading; }
